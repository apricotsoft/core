/**
 * @class InvalidArgument
 * @memberOf Apricot.Application
 */
class InvalidArgument extends Error {
    /**
     * Set argument
     *
     * @param {*} value
     */
    set argument(value) {
        this._argument = value;
    }

    /**
     * @param {string} message
     */
    constructor(message) {
        super(message);
    }
}

module.exports = InvalidArgument;