/**
 * @class CyclicInjection
 * @memberOf Apricot.Application
 */
class CyclicInjection extends Error {
    /**
     * @param {string} inject
     */
    constructor(inject) {
        super('Cyclic injection');

        this._inject = inject;
    }
}

module.exports = CyclicInjection;