const Tag = use('Tag');

/**
 * @class Member
 * @memberOf Apricot.Reflection
 */
class Member {
	/**
	 * @public
	 * @return {string}
	 */
	get name () {
		return this._data.name;
	}

	/**
	 * @public
	 * @return {*}
	 */
	get value () {
		return this._data.meta.code.value;
	}

	/**
	 * @constructor
	 * @memberOf Member
	 * @param data
	 */
	constructor (data) {
		this._data = data;
		this._tags = [];

		this._parse();
	}

	/**
	 * @private
	 * @return {void}
	 */
	_parse () {
		this._parseTga();
	}

	/**
	 * @private
	 * @return {void}
	 */
	_parseTga () {
		if (this._data.tags) {
			this._tags = this._data
				.tags
				.map(item => {
					return new Tag(item);
				});
		}
	}
}

module.exports = Member;