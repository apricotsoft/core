// const BaseRouteProvider = use('Apricot/Route/RouteProvider');

class RouteProvider /*extends BaseRouteProvider*/ {
    constructor() {
        this.app = application;
    }

    boot() {

    }

    register() {
        this.app.singleton('route', () => {
            return 1;
        });
    }
}

module.exports = RouteProvider;