class Inject {
	static get inject() {
		return ['App/Injectable'];
	}

	get testing() {
		return this._inject.test();
	}

	constructor (injectable) {
		this._inject = injectable;
	}

	test() {
		return 'testing'
	}
}

module.exports = Inject;